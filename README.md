# Data migration to OCI using a high-storage capacity device


## Introduction 

Oracle offers offline data transfer solutions that let you migrate data to Oracle Cloud Infrastructure using either a user-supplied storage device, or an Oracle-supplied transfer appliance. Here we focus on Oracle-supplied transfer appliance method.


## Data Transfer Appliance

The Data Transfer Appliance (import appliance) is a high-storage capacity device that is specially prepared to copy and upload data to Oracle Cloud Infrastructure. This lack of feasibility may stem from the upload dataset being too large, the internet connection being too slow, or an inability to dedicate the required internet bandwidth for the extended time that it takes to upload data. In general, you request a loan of one or more appliances from Oracle, copy your data onto it, and then ship it back to Oracle for upload. You can find detailed appliance specification [here](https://docs.oracle.com/en-us/iaas/Content/DataTransfer/Concepts/appliance_overview.htm), take into the consideration the following:

- The transfer appliance exposes an NFS mount point (NFS version 3, 4, or 4,1 to copy data to the transfer appliance using normal file system commands that copy data to an NFS target)
- Service availability depends on region – supported regions list [here](https://docs.oracle.com/en-us/iaas/Content/DataTransfer/Concepts/supported_regions.htm)
- Data Transfer appliance availability is based on inventory per region. Oracle distributes appliances on a first come, first serve basis based on customer request.
- Appliance storage capacity: 50TB of protected usable space (depends on Supported OCI Region)

When it comes to the security aspects, data is fully encrypted at each stage of the process including: 
- AES 256-bit encryption
- Data encrypted at rest in OCI Object Storage bucket (AES-256)
- Network communication is encrypted using Transit Layer Security (TLS)

Appliance is tamper-resistant and tamper-evident and chain of custody is maintained through the transfer process.


## How Data import works for Data Transfer Appliances?

As described above, DTA lets you to send your data as files to an Oracle transfer site. Operators at the Oracle transfer site upload the files into the designated Object Storage bucket in your tenancy. You are then free to move the uploaded data to other Oracle Cloud Infrastructure services as needed. The whole data import process for DTA is summarized below.
Please note that some steps require to use of the OCI CLI - not everything can be done via the OCI Console (check below which is required: CLI, Console, or can be both).
1. Request entitlement - **OCI CLI** or **Console**
2. E-sign Terms & Conditions via e-mail
3. Create Transfer Job and request appliance - **OCI CLI** or **Console**
4. Establish IAM policies - **OCI CLI** or **Console**
5. Create Object Storage bucket - **OCI CLI** or **Console**
6. Install CLI on Linux host*
7. Create configuration files
8. Receive appliance
9. Connect appliance to the network
10. Connect appliance via serial console and assign IP
11. Upload RSA key pairs - **OCI CLI** or **Console**
12. Set HTTP proxy and firewall access
13. Initialize authentication - **OCI CLI**
14. Retrieve encryption key - **OCI CLI**
15. Unlock appliance
16. Create NFS dataset
17. NFS mount dataset
18. Copy data to NFS dataset
19. ‘seal’ dataset to generate manifest
20. ‘finalize’ the appliance - **OCI CLI**
21. Ship the appliance back to Oracle
22. Oracle uploads data to bucket
23. Monitor data transfer progress - **OCI CLI** or **Console**

Notes:
- Oracle Cloud Infrastructure Command Line Interface (CLI) should be installed prior to step 1 if using CLI to request entitlement, create Transfer Job, or requesting Appliance.
- You can only run Oracle Cloud Infrastructure CLI commands from a Linux host. Appliance-based commands require validation that is only available on Linux hosts
- Maximum file size imported using DTA is 10 TB.

Detailed steps description can be found [here](https://docs.oracle.com/en-us/iaas/Content/DataTransfer/Tasks/appliance-entitlement-management.htm).

